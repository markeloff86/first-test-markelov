package lib;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by markeloff on 17.05.16.
 */
public class Init {
    private static WebDriver driver;
    private static HashMap<Object, List<String>> stash;

    public static HashMap<Object, List<String>> getStash() {
        if (null == stash)
            stash = new HashMap<>();
        return stash;
    }

    public static void setStash(Object key, List<String> listValue) {
        getStash().put(key, listValue);
    }

    public static void clearStash() {
        stash.clear();
    }

    public static WebDriver getDriver() throws Exception {
        if (null == driver) {
            createWebDriver();
        }
        return driver;
    }

    public static void setDriver(WebDriver driver) {
        Init.driver = driver;
    }

    public static void createWebDriver() throws Exception {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        String nameBrowser = getStash().get("browser").get(0);
        switch (nameBrowser) {
            case "Firefox":
                capabilities.setBrowserName("firefox");
                setDriver(new FirefoxDriver(capabilities));
                break;
            case "Chrome":
                File chromeDriver = new File("/home/markeloff/Документы/Стажировка СберТех/QA/my-app-test/src/test/resources/webdrivers/chromedriver");
                System.setProperty("webdriver.chrome.driver", chromeDriver.getAbsolutePath());
                capabilities.setBrowserName("chrome");
                setDriver(new ChromeDriver(capabilities));
                break;
        }


        driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(15, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }


}
