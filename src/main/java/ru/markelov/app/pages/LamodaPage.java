package ru.markelov.app.pages;


import lib.Init;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.allure.Allure;
import ru.yandex.qatools.allure.events.AddParameterEvent;

import java.util.List;

/**
 * Created by markeloff on 27.05.16.
 */
public class LamodaPage extends AnyPage {


    @FindBy(xpath = "//span[@class='popup__close']")
    private WebElement startFrame;

    @FindAll({@FindBy(xpath = "//a[@class='products-list-item__link link']")})
    private List<WebElement> listClothes;

    @FindAll({@FindBy(xpath = "//span[text()='Применить']")})
    private List<WebElement> buttonAccept;

    @FindBy(xpath = "//span[text()='Дальше']")
    private WebElement buttonNext;


    public WebElement getCountPage() throws Exception {
        WebElement countPage = Init.getDriver().findElement(By.xpath("//a[@class='button button_outline button_s paginator__num button_right']"));
        return countPage;
    }

    public WebElement getButtonNext() {
        return buttonNext;
    }

    public List<WebElement> getListClothes() {
        return listClothes;
    }

    public WebElement getButtonAccept(int i) {
        return buttonAccept.get(i);
    }

    public WebElement getStartFrame() {
        return startFrame;
    }


    public LamodaPage() throws Exception {
        PageFactory.initElements(Init.getDriver(), this);
        /*new WebDriverWait(Init.getDriver(), 30).until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//h2[text()='Женская одежда']")));*/

    }


    public void clickFilter(String nameElement, String nameFilter, int numberAccept) throws Exception {
        Thread.sleep(2000);
        WebElement element = Init.getDriver().findElement(By.xpath("//span[contains(text(),'" + nameElement + "')]"));
        if (!element.findElement(By.xpath("./..")).getAttribute("class").contains("button_disabled")) {
            click(element);
            Thread.sleep(1000);
            for (int i = 0; i < Init.getStash().get(nameFilter).size(); i++) {
                Allure.LIFECYCLE.fire(new AddParameterEvent("Применяем фильтр " + nameFilter + " со значением " + Init.getStash().get(nameFilter).get(i), "фильтр применен"));
                WebElement elementFilter = Init.getDriver().findElement(By.xpath("//label[text()='" + Init.getStash().get(nameFilter).get(i) + "']"));
                ((JavascriptExecutor) Init.getDriver()).executeScript("arguments[0].scrollIntoView();", elementFilter);
                try {
                    elementFilter.click();
                } catch (NoSuchElementException e) {

                }
            }

            buttonAccept.get(numberAccept).click();
            Thread.sleep(2000);

        }
    }

    public void insertCost(int numberAccept) throws Exception {
        Allure.LIFECYCLE.fire(new AddParameterEvent("Применяем фильтр по цене", "фильтр применен"));
        WebElement element = Init.getDriver().findElement(By.xpath("//span[contains(text(),'Цена')]"));
        if (!element.findElement(By.xpath("./..")).getAttribute("class").contains("button_disabled")) {
            click(element);
            Thread.sleep(1000);
            WebElement costStart = Init.getDriver().findElement(By.xpath("//input[@class='text-field range__value range__value_left']"));
            WebElement costFinish = Init.getDriver().findElement(By.xpath("//input[@class='text-field range__value range__value_right']"));
            setText(costStart, Init.getStash().get("costStart").get(0));
            setText(costFinish, Init.getStash().get("costFinish").get(0));
            Thread.sleep(5000);
            buttonAccept.get(numberAccept).click();
            Thread.sleep(2000);

        }


    }

    public void checkContainsClothes(String nameFilter, String nameFilterProperty) throws Exception {
        Allure.LIFECYCLE.fire(new AddParameterEvent("Проверка " + nameFilter + " на соответствие критерия", "данные товары соответствуют критериям"));
        WebElement parent = Init.getDriver().findElement(By.xpath("//span[text()='" + nameFilter + "']/.."));
        WebElement filter = parent.findElement(By.xpath("./span[2]"));
        int countContains = 0;
        for (int j = 0; j < Init.getStash().get(nameFilterProperty).size(); j++) {
            if (Init.getStash().get(nameFilterProperty).get(j).contains(filter.getText())) ;
            countContains++;
        }
        Assert.assertTrue("Нет заданного значения: " + Init.getStash().get(nameFilterProperty), countContains >= 1);


    }

    public void checkFilter() throws Exception {
        Allure.LIFECYCLE.fire(new AddParameterEvent("Проверка на соотвествие размера", "размер товара соответствует критерию"));
        for (int i = 0; i < listClothes.size(); i++) {
            List<WebElement> clothes = Init.getDriver().findElements(By.xpath("//a[@class='products-list-item__link link']"));
            List<WebElement> sizeParent = Init.getDriver().findElements(By.xpath("//div[@class='products-list-item__sizes']"));
            ((JavascriptExecutor) Init.getDriver()).executeScript("arguments[0].scrollIntoView();", clothes.get(i));
            Actions actions = new Actions(Init.getDriver());
            actions.moveToElement(clothes.get(i)).perform();
            int countContainsSize = 0;
            List<WebElement> sizes = sizeParent.get(i).findElements(By.tagName("a"));
            for (int j = 0; j < Init.getStash().get("size").size(); j++) {
                for (int k = 0; k < sizes.size(); k++) {
                    if (Init.getStash().get("size").get(j).contains(sizes.get(k).getText())) ;
                    countContainsSize++;
                }
            }
            Assert.assertTrue("Нет заданного размера: " + Init.getStash().get("size"), countContainsSize >= 1);
            clothes.get(i).click();
            checkContainsClothes("Цвет", "color");
            checkContainsClothes("Сезон", "season");
            checkContainsClothes("Стиль", "style");
            checkContainsClothes("Коллекция", "collection");
            Init.getDriver().navigate().back();
        }

    }

    public void chooseCategory() throws Exception {
        Allure.LIFECYCLE.fire(new AddParameterEvent("Выбор категории товара", "категория " + Init.getStash().get("category").get(0) + " выбрана"));
        WebElement categoryParent = Init.getDriver().findElement(By.xpath("//ul[@class='catalog-navigation catalog-navigation_subtree']"));
        ((JavascriptExecutor) Init.getDriver()).executeScript("arguments[0].scrollIntoView();", categoryParent);
        WebElement category = categoryParent.findElement(By.xpath("./li/a[text()='" + Init.getStash().get("category").get(0) + "']"));
        category.click();
        Thread.sleep(2000);
    }


    public void unchooseFilter(String nameFilter, String xpath, String nameFilter_find) throws Exception {
        Allure.LIFECYCLE.fire(new AddParameterEvent("Снятие ненужных фильтров для " + nameFilter, "фильтры убраны"));
        WebElement element = Init.getDriver().findElement(By.xpath(xpath));
        ((JavascriptExecutor) Init.getDriver()).executeScript("arguments[0].scrollIntoView();", element);
        Thread.sleep(4000);
        if (!element.findElement(By.xpath("./div/span[1]")).getAttribute("class").contains("button_disabled")) {
            click(element);
            Thread.sleep(2000);
            for (int i = 0; i < Init.getStash().get(nameFilter).size(); i++) {
                String unchooseElement = Init.getStash().get(nameFilter).get(i);
                if (!unchooseElement.equals(Init.getStash().get(nameFilter_find).get(0))) {
                    try {
                        WebElement elementFilter = Init.getDriver().findElement(By.xpath("//label[text()='" + unchooseElement + "']"));
                        ((JavascriptExecutor) Init.getDriver()).executeScript("arguments[0].scrollIntoView();", elementFilter);
                        click(elementFilter);
                    } catch (NoSuchElementException e) {
                        Allure.LIFECYCLE.fire(new AddParameterEvent("Проверка " + nameFilter, "данного параметра не существует для выбранных критериев"));
                    }

                }

            }
        }
    }


    public void chooseFilterCost() throws Exception {
        Allure.LIFECYCLE.fire(new AddParameterEvent("Сортировка товаров по " + Init.getStash().get("cost_find").get(0) + " цене", "товары отсортированы"));
        WebElement elementLocator = Init.getDriver().findElement(By.xpath("//div[@class='search__button-logo']"));
        ((JavascriptExecutor) Init.getDriver()).executeScript("arguments[0].scrollIntoView();", elementLocator);
        Thread.sleep(1000);
        WebElement filterSort = Init.getDriver().findElement(By.xpath("//span[text()='Сортировать:']/span/span"));
        click(filterSort);
        Thread.sleep(1000);

        if (Init.getStash().get("cost_find").get(0).equals("наименьшая")) {
            WebElement element = Init.getDriver().findElement(By.xpath("//span[text()='Сортировать:']/span/ul/li[1]"));
            click(element);
        }
        if (Init.getStash().get("cost_find").get(0).equals("наибольшая")) {
            WebElement element = Init.getDriver().findElement(By.xpath("//span[text()='Сортировать:']/span/ul/li[1]"));
            click(element);
        }

    }

    public void getFindCloth() {

        String findCloth = listClothes.get(0).findElement(By.xpath("./img")).getAttribute("alt");
        WebElement price = listClothes.get(0).findElement(By.xpath("./span"));
        Allure.LIFECYCLE.fire(new AddParameterEvent("Искомый товар", findCloth));
        Allure.LIFECYCLE.fire(new AddParameterEvent("Цена", price.getText()));
    }


}







