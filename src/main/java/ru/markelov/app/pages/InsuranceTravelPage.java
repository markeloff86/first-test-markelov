package ru.markelov.app.pages;

import lib.Init;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


/**
 * Created by markeloff on 20.05.16.
 */
public class InsuranceTravelPage extends AnyPage {
    @FindBy(name = "startDate")
    private WebElement startDate;

    @FindBy(xpath = "//h2[text()='Страхование путешественников']")
    private WebElement initElement;

    @FindBy(xpath = "//span[@class='ng-binding ng-scope b-dropdown-title']")
    private WebElement textLocation;

    @FindBy(xpath = "//fieldset[@class='b-form-fieldset b-form-fieldset-splash b-form-margtop-fieldset']")
    private WebElement textBig;

    @FindBy(name = "finishDate")
    private WebElement finishDate;

    @FindBy(xpath = "//div[@id='views']/form/section/section/section[1]/div[4]/fieldset/span/input")
    private WebElement checkBox;

    @FindBy(xpath = "//fieldset[@class='b-form-fieldset-splash']/span[1]/input")
    private WebElement countElement;

    @FindBy(name = "insuredCount60")
    private WebElement countPeople;

    @FindBy(name = "insuredCount2")
    private WebElement countPeople1;

    @FindBy(name = "insuredCount70")
    private WebElement countChuld;

    @FindBy(xpath = "//div[@class='b-form-prog-box b-form-active-box']")
    private WebElement colorFrame;

    @FindBy(xpath = "//div[@id='views']/form/section/section/section[3]/div/div[3]")
    private WebElement specialFrame;

    @FindBy(xpath = "//div[@id='views']/form/section/section/section[3]/div/div[4]")
    private WebElement privateFrame;

    @FindBy(xpath = "//div[@id='views']/form/section/section/section[5]/div/dl[2]/dd[1]/span[1]")
    private WebElement totalCost;

    @FindBy(xpath = "//div[@id='views']/form/section/section/section[3]/div/div[1]")
    private WebElement sportFrame;

    @FindBy(xpath = "//div[@id='views']/form/section/section/section[3]/div/div[5]")
    private WebElement providentFrame;

    @FindBy(xpath = "//div[@id='views']/form/section/section/section[3]/div/div[2]")
    private WebElement baggage;

    @FindBy(xpath = "//ul[@class='nav nav-tabs nav-justified']/li[2]")
    private WebElement decor;

    @FindBy(xpath = "//ul[@class='nav nav-tabs nav-justified']/li[3]")
    private WebElement accept;

    @FindBy(xpath = "//div[@id='views']/form/section/section/section[2]/div[1]/div[1]/div")
    private WebElement min;

    @FindBy(xpath = "//div[@id='views']/form/section/section/section[3]/div/div[1]/span[1]")
    private WebElement sportTextName;

    @FindBy(xpath = "//div[@id='views']/form/section/section/section[3]/div/div[1]/ul/li[1]")
    private WebElement sportText1;

    @FindBy(xpath = "//div[@id='views']/form/section/section/section[3]/div/div[1]/ul/li[2]")
    private WebElement sportText2;

    @FindBy(xpath = "//div[@id='views']/form/section/section/section[3]/div/div[1]/ul/li[3]")
    private WebElement sportText3;

    @FindBy(xpath = "//div[@id='views']/form/section/section/section[3]/div/div[1]/span[4]")
    private WebElement costSport;


    public InsuranceTravelPage() throws Exception {
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), 30).until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//h2[text()='Страхование путешественников']")));

    }


    public void checkFilingFields() {
        try {
            Thread.sleep(5000);
            Assert.assertEquals("Весь мир, кроме США и РФ",textLocation.getText());
            Assert.assertEquals(Init.getStash().get("startDate").get(0),startDate.getAttribute("value"));
            Assert.assertEquals(Init.getStash().get("finishDate").get(0),finishDate.getAttribute("value"));
            Assert.assertFalse(checkBox.isSelected());
            Assert.assertEquals("15",countElement.getAttribute("value"));
            Assert.assertEquals("1",countPeople.getAttribute("value"));
            Assert.assertEquals("0",countPeople1.getAttribute("value"));
            Assert.assertEquals("0",countChuld.getAttribute("value"));
            Assert.assertEquals("rgba(255, 167, 21, 1)",colorFrame.getCssValue("border-top-color"));
            Assert.assertEquals("rgba(235, 237, 236, 1)",sportFrame.getCssValue("border-top-color"));
            Assert.assertEquals("rgba(235, 237, 236, 1)",specialFrame.getCssValue("border-top-color"));
            Assert.assertEquals("rgba(235, 237, 236, 1)",privateFrame.getCssValue("border-top-color"));
            Assert.assertEquals("rgba(235, 237, 236, 1)",providentFrame.getCssValue("border-top-color"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void availabilityTabs() {
        try {
            Assert.assertTrue(decor.getAttribute("class").contains("disabled"));
            Assert.assertTrue(accept.getAttribute("class").contains("disabled"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void checkValueTotalCost() {
        try {
            Assert.assertTrue(totalCost.getText().contains(Init.getStash().get("totalCost").get(0)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void chooseValueMin() {
        try {
            click(min);
            Thread.sleep(5000);
            Assert.assertTrue(min.findElement(By.tagName("span")).getAttribute("class").equals("b-form-prog-box-check-pos b-checked-checkbox-field"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void checkValueTotalCost2() {
        try {
            Assert.assertTrue(totalCost.getText().contains(Init.getStash().get("totalCostAfterClickMin").get(0)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void uncheckBaggage() {
        try {
            click(baggage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void checkSport() {
        try {
            click(sportFrame);
            Thread.sleep(5000);
            Assert.assertTrue(totalCost.getText().contains(Init.getStash().get("totalCostAfterCheckSport").get(0)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void checkTextSport() {
        try {
            Assert.assertTrue(sportTextName.getText().contains("Спортивный"));
            Assert.assertTrue(sportText1.getText().contains("Активные виды спорта"));
            Assert.assertTrue(sportText2.getText().contains("Защита спортинвентаря"));
            Assert.assertTrue(sportText3.getText().contains("Ski-pass / Лавина"));
            Thread.sleep(5000);
            Assert.assertTrue(costSport.getText().contains( Init.getStash().get("costSport").get(0)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void chooseProvidentFrame() {
        try {
            click(providentFrame);
            Thread.sleep(5000);
            Assert.assertTrue(totalCost.getText().contains( Init.getStash().get("totalCostWithoutBaggage").get(0)));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void chooseBaggage() {
        try {
            click(baggage);
            click(sportFrame);
            Thread.sleep(5000);
            Assert.assertTrue(totalCost.getText().contains( Init.getStash().get("totalCostWithBaggageProvident").get(0)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}