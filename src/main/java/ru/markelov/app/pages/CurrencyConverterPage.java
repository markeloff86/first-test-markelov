package ru.markelov.app.pages;

import lib.Init;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.math.BigDecimal;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by markeloff on 21.05.16.
 */
public class CurrencyConverterPage extends AnyPage {
    @FindBy(xpath = "//div[@class='currency-converter-widget']/div[1]/span")
    private WebElement elementDate;

    @FindBy(xpath = "//label[text()='Поменять']")
    private WebElement textChange;

    @FindBy(xpath = "//label[text()='На']")
    private WebElement textNa;


    @FindBy(xpath = "//input[@id='from']")
    private WebElement inputFrom;

    @FindBy(xpath = "//input[@id='to']")
    private WebElement inputTo;

    @FindBy(xpath = "//div[@class='currency-converter-result']/span[1]")
    private WebElement span1;

    @FindBy(xpath = "//div[@class='currency-converter-result']/span[3]")
    private WebElement span_value1;

    @FindBy(xpath = "//div[@class='currency-converter-result']/span[4]")
    private WebElement span_equals;

    @FindBy(xpath = "//div[@class='currency-converter-result']/span[5]")
    private WebElement span_kurs;

    @FindBy(xpath = "//div[@class='currency-converter-result']/span[7]")
    private WebElement span_value2;

    @FindBy(xpath = "//ul[@class='select2-results ps-container']/li[1]")
    private WebElement selectorUSDChange;

    @FindBy(xpath = "//div[@class='SBR-personal-rates']/table/tbody/tr[1]/td[2]")
    private WebElement course_EUR_buy;

    @FindBy(xpath = "//div[@class='SBR-personal-rates']/table/tbody/tr[1]/td[3]")
    private WebElement course_EUR_sell;

    @FindBy(xpath = "//div[@class='SBR-personal-rates']/table/tbody/tr[2]/td[3]")
    private WebElement course_USD_sell;

    @FindBy(xpath = "//div[@class='SBR-personal-rates']/table/tbody/tr[2]/td[2]")
    private WebElement course_USD_buy;


    @FindBy(xpath = "//div[@id='select2-drop']/ul/li[2]")
    private WebElement selectorEURNa;

    @FindBy(xpath = "//div[@class='bp-widget-body']/div/div/form/div[1]/div/div")
    private WebElement listChange;

    @FindBy(xpath = "//div[@class='bp-widget-body']/div/div/form/div[2]/div/div")
    private WebElement listNa;

    @FindBy(xpath = "//div[@id='select2-drop']/ul/li[1]")
    private WebElement selectUSDNa;


    public CurrencyConverterPage() throws Exception {
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), 30).until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//span[@class='personalized-widget-title aa-widget-head-draggable currency-icon']")));

    }


    private static DateFormatSymbols myDateFormatSymbols = new DateFormatSymbols() {

        @Override
        public String[] getMonths() {
            return new String[]{"января", "февраля", "марта", "апреля", "мая", "июня",
                    "июля", "августа", "сентября", "октября", "ноября", "декабря"};
        }

    };

    public void checkDate() {
        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", myDateFormatSymbols);
        Assert.assertEquals(dateFormat.format(currentDate), elementDate.getText());
    }

    private BigDecimal round(double value, int digits) {
        return new BigDecimal("" + value).setScale(digits, BigDecimal.ROUND_HALF_UP); //метод округления
    }

    public void checkComponents() {
        Assert.assertEquals("Поменять", textChange.getText());
        Assert.assertEquals("На", textNa.getText());
        Assert.assertTrue(listChange.isDisplayed());
        Assert.assertTrue(listNa.isDisplayed());
        Assert.assertTrue(inputFrom.isDisplayed());
        Assert.assertTrue(inputTo.isDisplayed());
        Assert.assertEquals("1", span1.getText());
        Assert.assertEquals("RUB", span_value1.getText());
        Assert.assertEquals("=", span_equals.getText());
        double courseRubEur = 1 / Double.parseDouble(course_EUR_sell.getText());
        Assert.assertEquals(String.valueOf(round(courseRubEur, 4)), span_kurs.getText()); //округляем до 4 знаков после запятой
        Assert.assertEquals("EUR", span_value2.getText());
    }

    public void insertValue() {
        setText(inputFrom, "34");
    }

    public void checkValue() throws InterruptedException {
        Thread.sleep(2000);
        double insertValue = Double.parseDouble(inputFrom.getAttribute("value"));
        System.out.println(insertValue);
        double course = Double.parseDouble(String.valueOf(round(1 / Double.parseDouble(course_EUR_sell.getText()), 10)));
        System.out.println(course);
        Assert.assertEquals(String.valueOf(round(insertValue * course, 2)), inputTo.getAttribute("value"));
    }

    public void insertUsdValue() throws Exception {
        listChange.click();
        Thread.sleep(2000);
        selectorUSDChange.click();
        Thread.sleep(2000);
        setText(inputFrom, "10023");
        Thread.sleep(2000);
        listNa.click();
        Thread.sleep(2000);
        selectorEURNa.click();
        Thread.sleep(3000);

    }

    public void checkValueEUR() throws Exception {
        double insertValue = Double.parseDouble(inputFrom.getAttribute("value"));
        System.out.println(insertValue);
        double course = Double.parseDouble(String.valueOf(Double.parseDouble(course_USD_buy.getText()) / Double.parseDouble(course_EUR_sell.getText())));
        System.out.println(course);
        System.out.println(String.valueOf(round(insertValue * course, 2)));
        System.out.println(inputTo.getAttribute("value"));
        Assert.assertEquals(String.valueOf(round(insertValue * course, 2)), inputTo.getAttribute("value").replaceAll(" ",""));
        click(listChange);
        Thread.sleep(2000);
        selectorUSDChange.click();
        Thread.sleep(2000);
        setText(inputFrom, "5");
        Thread.sleep(2000);
        listNa.click();
        Thread.sleep(2000);
        selectUSDNa.click();
        Thread.sleep(2000);
        Assert.assertEquals("RUB", listChange.getText());
        Assert.assertEquals("5", inputFrom.getAttribute("value"));
        double insertValue2 = Double.parseDouble(inputFrom.getAttribute("value").replaceAll(" ",""));
        double course2 = Double.parseDouble(String.valueOf(round(1 / Double.parseDouble(course_USD_sell.getText()), 10)));
        Assert.assertEquals(String.valueOf(round(insertValue2 * course2, 2)), inputTo.getAttribute("value").replaceAll(" ",""));
    }


    public void insertUSD() throws Exception {
        click(listChange);
        Thread.sleep(2000);
        selectorUSDChange.click();
        Thread.sleep(2000);
       setText(inputFrom, "5");
        Thread.sleep(2000);
        listNa.click();
        Thread.sleep(2000);
        selectUSDNa.click();
        Thread.sleep(2000);
        Assert.assertEquals("RUB", listChange.getText());
        Assert.assertEquals("5", inputFrom.getAttribute("value"));
        double insertValue = Double.parseDouble(inputFrom.getAttribute("value").replaceAll(" ",""));
        double course = Double.parseDouble(String.valueOf(round(1 / Double.parseDouble(course_USD_sell.getText()), 10)));
        Assert.assertEquals(String.valueOf(round(insertValue * course, 2)), inputTo.getAttribute("value"));


    }
}
