package ru.markelov.app.pages;

import lib.Init;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by markeloff on 23.05.16.
 */
public class ATMPage extends AnyPage {
    @FindAll({@FindBy(xpath = "//ul[@id='branchListItems']/li")})
    private List<WebElement> allElementsATM;

    @FindAll({@FindBy(xpath = "//div[@class='branch-list-item-block']//p[1]")})
    private List<WebElement> allElementsLocation;

    @FindBy (xpath ="//label[text()='Платёжные устройства']")
    private  WebElement checkBoxPaymentDevice;

    @FindBy (xpath = "//button[@class='sbf_button show-more']")
    private WebElement btnShowMore;

    @FindBy(xpath = "//label[text()='Отделения']")
    private WebElement office;

    private static int countLocation;

    public ATMPage() throws Exception {
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), 30).until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//h1[text()='Отделения и банкоматы']")));

    }

    public void checkGreenSign() {
        for (WebElement element : allElementsATM)
            Assert.assertEquals("item-list-icon filial", element.findElement(By.tagName("span")).getAttribute("class"));
    }

    public void checkMinLocation() {
        Assert.assertTrue(allElementsATM.size() >= 1);
    }

    public void checkOrderLocation() {
        Pattern pat = Pattern.compile("[0-9]+(.[0-9]+)?");
        ArrayList<Double> listDistance = new ArrayList<>();
        Pattern patNameDistance = Pattern.compile("[к]?[м]{1}");
        ArrayList<String> nameDistance = new ArrayList<>();
        for (WebElement element : allElementsLocation) {
            Matcher matcher = pat.matcher(element.getText());
            Matcher matcherNameDistance = patNameDistance.matcher(element.getText());
            while (matcher.find()) {
                listDistance.add((Double.parseDouble(matcher.group())));
            }
            while (matcherNameDistance.find()) {
                nameDistance.add((matcherNameDistance.group()));
            }
        }

        for (int i = 0; i < listDistance.size(); i++) {
            if (nameDistance.get(i).equals("км")) {
                double number = listDistance.get(i) * 1000;
                listDistance.remove(i);
                listDistance.add(i, number);
            }

        }
        for (int i = 0; i < listDistance.size() - 1; i++) {
            Assert.assertTrue(listDistance.get(i) <= listDistance.get(i + 1));
        }
        countLocation=listDistance.size();


    }

    public void chooseCheckBoxPaymentDevice() throws Exception {
        click(checkBoxPaymentDevice);
        Thread.sleep(3000);
        int countPayment=0;
        for(WebElement element:allElementsATM){
            if (element.findElement(By.tagName("span")).getAttribute("class").equals("item-list-icon itt"))
                countPayment++;
        }
        Assert.assertTrue(countPayment>=1);


    }


    public void pushButtonShowMore() throws Exception {
        click(btnShowMore);
        Thread.sleep(2000);
        Assert.assertTrue(countLocation<allElementsATM.size());
        checkOrderLocation();
    }

    public void uncheckOffice() throws Exception {
        click(office);
        Thread.sleep(3000);
        int countPayment=0;
        for(WebElement element:allElementsATM){
            if (element.findElement(By.tagName("span")).getAttribute("class").equals("item-list-icon itt"))
                countPayment++;
        }
        Assert.assertEquals(allElementsATM.size(),countPayment);



    }
}
