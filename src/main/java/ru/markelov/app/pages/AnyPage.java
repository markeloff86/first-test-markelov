package ru.markelov.app.pages;

import lib.Init;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by markeloff on 20.05.16.
 */
public class AnyPage {
    public AnyPage() throws Exception {
        PageFactory.initElements(Init.getDriver(), this);
        waitPageToLoad();
    }


    void waitPageToLoad() throws Exception {
        new WebDriverWait(Init.getDriver(), 30).until((ExpectedCondition<Boolean>) wd ->
                ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
    }


    public void click(WebElement element) throws Exception {
        new WebDriverWait(Init.getDriver(), 30)
                .until(ExpectedConditions.elementToBeClickable(element));
        element.click();
        System.out.println("Click to element " + element.getText());
    }

    public void click(By by) throws Exception {
        WebElement element = Init.getDriver().findElement(by);
        click(element);
    }

    public void setText(WebElement element, String text) {
        element.clear();
        element.sendKeys(text);

    }

    public void setText(By by, String text) throws Exception {
        WebElement element = Init.getDriver().findElement(by);
        setText(element, text);
    }


}

