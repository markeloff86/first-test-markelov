package stepDefinitions;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import lib.Init;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import ru.markelov.app.pages.ATMPage;
import ru.markelov.app.pages.CurrencyConverterPage;
import ru.markelov.app.pages.InsuranceTravelPage;
import ru.markelov.app.pages.LamodaPage;
import ru.yandex.qatools.allure.Allure;
import ru.yandex.qatools.allure.events.AddParameterEvent;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;


/**
 * Created by markeloff on 18.05.16.
 */
public class CommonStepDefinitions {


    @Before
    public void before() {
        FileInputStream beforeInput = null;
        try {
            beforeInput = new FileInputStream("/home/markeloff/Документы/Стажировка СберТех/QA/my-app-test/src/test/java/config/app");
        } catch (FileNotFoundException e) {
            Allure.LIFECYCLE.fire(new AddParameterEvent("Загружаем файл со свойствами", "файл не найдет"));
        }
        Properties prop = new Properties();
        try {
            prop.load(beforeInput);
        } catch (IOException e) {
            Allure.LIFECYCLE.fire(new AddParameterEvent("Загрузка свойств из файла", "свойства не загружены"));
        }
        loadPropertyToStash("material", prop);
        loadPropertyToStash("material_find", prop);
        loadPropertyToStash("brand", prop);
        loadPropertyToStash("brand_find", prop);
        loadPropertyToStash("color", prop);
        loadPropertyToStash("color_find", prop);
        loadPropertyToStash("size_find", prop);
        loadPropertyToStash("size", prop);
        loadPropertyToStash("season", prop);
        loadPropertyToStash("season_find", prop);
        loadPropertyToStash("collection", prop);
        loadPropertyToStash("collection_find", prop);
        loadPropertyToStash("style", prop);
        loadPropertyToStash("style_find", prop);
        loadPropertyToStash("costStart", prop);
        loadPropertyToStash("costFinish", prop);
        loadPropertyToStash("cost_find", prop);
        loadPropertyToStash("category", prop);
        loadPropertyToStash("browser", prop);
        loadPropertyToStash("URL", prop);
        loadPropertyToStash("startDate", prop);
        loadPropertyToStash("finishDate", prop);
        loadPropertyToStash("totalCost", prop);
        loadPropertyToStash("totalCostAfterClickMin", prop);
        loadPropertyToStash("totalCostAfterCheckSport", prop);
        loadPropertyToStash("costSport", prop);
        loadPropertyToStash("totalCostWithoutBaggage", prop);
        loadPropertyToStash("totalCostWithBaggageProvident", prop);
    }


    public void loadPropertyToStash(String nameFilter, Properties prop) {
        try {
            String filter = prop.getProperty(nameFilter);
            List<String> listFilter = Arrays.asList(filter.split(";"));
            Init.setStash(nameFilter, listFilter);
            Allure.LIFECYCLE.fire(new AddParameterEvent("Загрузка параметра "+nameFilter+" из файла", "параметр загружен"));
        } catch (Exception e) {
            Allure.LIFECYCLE.fire(new AddParameterEvent("Загрузка параметра " + nameFilter, "параметр не задан"));
        }

    }


    @After
    public void exitBrowser() {
        try {
            Init.clearStash();
            Init.getDriver().quit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Given("go to link")
    public void goToLink() {
        Allure.LIFECYCLE.fire(new AddParameterEvent("Переход по ссылке", Init.getStash().get("URL").get(0)));
        try {
            Init.getDriver().get(Init.getStash().get("URL").get(0));
        } catch (Exception e) {
            Allure.LIFECYCLE.fire(new AddParameterEvent("Переходим по ссылке", "переход не удался"));
        }


    }

    @Given("check filing fields")
    public void checkFilingFields() throws Exception {
        InsuranceTravelPage insuranceTravelPage = new InsuranceTravelPage();
        insuranceTravelPage.checkFilingFields();

    }

    @Given("check availability tabs")
    public void availabilityTabs() throws Exception {
        InsuranceTravelPage insuranceTravelPage = new InsuranceTravelPage();
        insuranceTravelPage.availabilityTabs();
    }

    @Given("check value of total cost")
    public void checkValueTotalCost() throws Exception {
        InsuranceTravelPage insuranceTravelPage = new InsuranceTravelPage();
        insuranceTravelPage.checkValueTotalCost();

    }

    @Given("choose frame min")
    public void chooseValueMin() throws Exception {
        InsuranceTravelPage insuranceTravelPage = new InsuranceTravelPage();
        insuranceTravelPage.chooseValueMin();
    }

    @Given("check value of total cost 2")
    public void checkValueTotalCost2() throws Exception {
        InsuranceTravelPage insuranceTravelPage = new InsuranceTravelPage();
        insuranceTravelPage.checkValueTotalCost2();
    }

    @Given("unchoose frame baggage")
    public void uncheckBaggage() throws Exception {
        InsuranceTravelPage insuranceTravelPage = new InsuranceTravelPage();
        insuranceTravelPage.uncheckBaggage();
    }

    @Given("choose sport frame")
    public void checkSport() throws Exception {
        InsuranceTravelPage insuranceTravelPage = new InsuranceTravelPage();
        insuranceTravelPage.checkSport();
    }

    @Given("checking text for sport frame")
    public void checkTextSport() throws Exception {
        InsuranceTravelPage insuranceTravelPage = new InsuranceTravelPage();
        insuranceTravelPage.checkTextSport();
    }

    @Given("choose provident frame")
    public void chooseProvidentFrame() throws Exception {
        InsuranceTravelPage insuranceTravelPage = new InsuranceTravelPage();
        insuranceTravelPage.chooseProvidentFrame();
    }

    @Given("choose baggage frame")
    public void chooseBaggage() throws Exception {
        InsuranceTravelPage insuranceTravelPage = new InsuranceTravelPage();
        insuranceTravelPage.chooseBaggage();
    }

    //Тест2

    @Given("Checking date")
    public void checkDate() throws Exception {
        CurrencyConverterPage currencyConverterPage = new CurrencyConverterPage();
        currencyConverterPage.checkDate();
    }

    @Given("Checking components")
    public void checkComponents() throws Exception {
        CurrencyConverterPage currencyConverterPage = new CurrencyConverterPage();
        currencyConverterPage.checkComponents();
    }

    @Given("Insert value")
    public void insertValue() throws Exception {
        CurrencyConverterPage currencyConverterPage = new CurrencyConverterPage();
        currencyConverterPage.insertValue();
    }

    @Given("Checking value after insert")
    public void checkValue() throws Exception {
        CurrencyConverterPage currencyConverterPage = new CurrencyConverterPage();
        currencyConverterPage.checkValue();
    }

    @Given("Insert USD value")
    public void insertUsdValue() throws Exception {
        CurrencyConverterPage currencyConverterPage = new CurrencyConverterPage();
        currencyConverterPage.insertUsdValue();
    }

    @Given("Checking EUR value")
    public void checkValueEUR() throws Exception {
        CurrencyConverterPage currencyConverterPage = new CurrencyConverterPage();
        currencyConverterPage.checkValueEUR();
    }

    @Given("Insert USD value2")
    public void insertUSD() throws Exception {
        CurrencyConverterPage currencyConverterPage = new CurrencyConverterPage();
        currencyConverterPage.insertUSD();


    }


    //Тест 3
    @Given("Checking green sign")
    public void checkGreenSign() throws Exception {
        ATMPage atmPage = new ATMPage();
        atmPage.checkGreenSign();

    }

    @Given("Checking location ATM")
    public void checkMinLocation() throws Exception {
        ATMPage atmPage = new ATMPage();
        atmPage.checkMinLocation();
    }

    @Given("Checking order location")
    public void checkOrderLocation() throws Exception {
        ATMPage atmPage = new ATMPage();
        atmPage.checkOrderLocation();
    }

    @Given("Choose check box of payment device")
    public void chooseCheckBoxPaymentDevice() throws Exception {
        ATMPage atmPage = new ATMPage();
        atmPage.chooseCheckBoxPaymentDevice();
    }

    @Given("Push button show more")
    public void pushButtonShowMore() throws Exception {
        ATMPage atmPage = new ATMPage();
        atmPage.pushButtonShowMore();
    }

    @Given("Unchecking check box of office")
    public void uncheckOffice() throws Exception {
        ATMPage atmPage = new ATMPage();
        atmPage.uncheckOffice();

    }

    //финальный тест

    @Given("Close start frame")
    public void closeStartFrame() throws Exception {
        Allure.LIFECYCLE.fire(new AddParameterEvent("Убираем рекламу", "реклама убрана"));
        LamodaPage lamodaPage = new LamodaPage();
        WebElement element = lamodaPage.getStartFrame();
        lamodaPage.click(element);

    }

    @Given("Choose filters")
    public void chooseFilters() throws Exception {
        LamodaPage lamodaPage = new LamodaPage();
        if (Init.getStash().get("material") != null) {
            lamodaPage.clickFilter("Материал", "material", 0);
        }
        if (Init.getStash().get("color") != null) {
            lamodaPage.clickFilter("Цвет", "color", 1);
        }
        if (Init.getStash().get("size") != null) {
            lamodaPage.clickFilter("Размер", "size", 2);

        }
        if (Init.getStash().get("brand") != null) {
            lamodaPage.clickFilter("Бренд", "brand", 3);
        }
        if (Init.getStash().get("season") != null) {
            lamodaPage.clickFilter("Сезон", "season", 4);
        }
        if (Init.getStash().get("collection") != null) {
            lamodaPage.clickFilter("Коллекция", "collection", 5);
        }
        if (Init.getStash().get("style") != null) {
            lamodaPage.clickFilter("Cтиль", "style", 6);
        }
        if (Init.getStash().get("costStart") != null && Init.getStash().get("costFinish") != null) {
            lamodaPage.insertCost(7);
        }
        Thread.sleep(1000);


    }


    @Given("Check for a match with the criteria")
    public void check() throws Exception {
        LamodaPage lamodaPage = new LamodaPage();
        int countPage = 0;
        try {
            countPage = Integer.parseInt(lamodaPage.getCountPage().getText());

        } catch (NoSuchElementException e) {
            countPage = 1;
        } finally {
            for (int i = 0; i < countPage; i++) {
                //lamodaPage.checkFilter(); проверка каждого товара на соответствие критериям (очень долго, заходит на страницу каждого товара)
                Thread.sleep(2000);
                if (i < countPage - 1) {
                    ((JavascriptExecutor) Init.getDriver()).executeScript("arguments[0].scrollIntoView();", lamodaPage.getButtonNext());
                    Thread.sleep(3000);
                    lamodaPage.getButtonNext().click();
                }
                Thread.sleep(2000);
            }
        }

    }

    @Given("Choose category")
    public void chooseCategory() throws Exception {
        LamodaPage lamodaPage = new LamodaPage();
        lamodaPage.chooseCategory();
        Thread.sleep(2000);
    }


    @Given("Unchoose filter")
    public void unchooseFilter() throws Exception {
        LamodaPage lamodaPage = new LamodaPage();
        if (Init.getStash().get("material_find") != null) {
            lamodaPage.unchooseFilter("material","//div[@class='multifilter-new multifilter-new_materials multifilter-new_active']","material_find");
            lamodaPage.getButtonAccept(0).click();
        }
        if (Init.getStash().get("color_find") != null) {
            lamodaPage.unchooseFilter("color","//div[@class='multifilter-new multifilter-new_colors multifilter-new_active']","color_find");
            lamodaPage.getButtonAccept(1).click();
        }
        if (Init.getStash().get("size_find") != null) {
            lamodaPage.unchooseFilter("size","//div[@class='multifilter-new multifilter-new_size_values multifilter-new_active']","size_find");
            lamodaPage.getButtonAccept(2).click();
        }
        if (Init.getStash().get("brand_find") != null) {
            lamodaPage.unchooseFilter("brand","//div[@class='multifilter-new multifilter-new_brands multifilter-new_active']","brand_find");
            lamodaPage.getButtonAccept(3).click();
        }

        if (Init.getStash().get("season_find") != null) {
            lamodaPage.unchooseFilter("season","//div[@class='multifilter-new multifilter-new_seasons multifilter-new_active']","season_find");
            lamodaPage.getButtonAccept(4).click();
        }
        if (Init.getStash().get("collection_find") != null) {
            lamodaPage.unchooseFilter("collection","div[@class='multifilter-new multifilter-new_collections multifilter-new_active']","collection_find");
            lamodaPage.getButtonAccept(5).click();
        }
        if (Init.getStash().get("style_find") != null) {
            lamodaPage.unchooseFilter("style","div[@class='multifilter-new multifilter-new_styles multifilter-new_active']","style_find");
            lamodaPage.getButtonAccept(6).click();
        }
    }


    @Given("Choose filter cost")
    public void chooseFilterCost() throws Exception {
        LamodaPage lamodaPage = new LamodaPage();
        lamodaPage.chooseFilterCost();
        Thread.sleep(10000);
    }


    @Given("Get find cloth")
    public void getFindCloth() throws Exception {
        LamodaPage lamodaPage = new LamodaPage();
        lamodaPage.getFindCloth();
    }


}
