Feature: Вводные тесты

  @Test1
  Scenario: Checking total cost insurance
    Given go to link
    Given check filing fields
    Given check availability tabs
    Given check value of total cost
    Given choose frame min
    Given unchoose frame baggage
    Given choose sport frame
    Given checking text for sport frame
    Given choose provident frame
    Given choose baggage frame

  @Test2
  Scenario:Work of converter
    Given go to link
    Given Checking date
    Given Checking components
    Given Insert value
    Given Checking value after insert
    Given Insert USD value
    Given Checking EUR value


  @Test3
  Scenario:Find ATM
    Given go to link
    Given Checking green sign
    Given Checking location ATM
    Given  Checking order location
    Given Choose check box of payment device
    Given Checking order location
    Given Push button show more
    Given Unchecking check box of office
    Given Checking order location


  @LamodaTest
  Scenario: Start
    Given go to link
    Given Close start frame
    Given Choose category
    Given Choose filters
    Given Check for a match with the criteria
    Given Unchoose filter
    Given Choose filter cost
    Given Get find cloth
