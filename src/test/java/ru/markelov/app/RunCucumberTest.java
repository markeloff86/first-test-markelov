package ru.markelov.app;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


/**
 * Created by markeloff on 25.05.16.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features/",
        glue = {"stepDefinitions"},
        tags = "@Test1")
public class RunCucumberTest {

}
